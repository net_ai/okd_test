package ru.alfastrah.oms.test.demo.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @RequestMapping("/")
    public String sayHello(@RequestParam(value = "name", defaultValue = "unnamed") String name ){
        return "Hello "+name+" !";
    }
}
